Installation
============

The ``setuptools`` way
----------------------

#. Make sure you have ``setuptools`` installed.
#. Download the whole package (including setup.py).
#. Activate your virtual environment of choice.
#. Run ``python setup.py install``

Manually
--------

#. Download the whole package.
#. Install dependencies:

    #. ``numpy`` if you want to use ``evo.sr.MultiGeneGeSrFitness``
